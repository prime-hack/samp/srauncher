#include "myserverlist.h"
#include <QDir>

#include <QMessageBox>

MyServerList::MyServerList( QListWidget *srvList, QObject *parent ) : QObject( parent ) {
	if ( srvList == nullptr ) throw std::runtime_error( "MyServerList: QToolBox not created" );
	this->srvList = srvList;

	QDir srvDir( "./SRauncher/srv" );
	if ( !srvDir.exists() ) srvDir.mkdir( "./SRauncher/srv" );

	settings  = new QSettings( "./SRauncher/srv/" + srvList->objectName(), QSettings::IniFormat );
	groupName = srvList->objectName();

	// TODO: read servers and create srvData
	// TODO: read servers and paste in ToolBox
}

MyServerList::~MyServerList() {
	if ( groupName == srvList->objectName() ) return;

	settings->sync();
	delete settings;

	QFile( "./SRauncher/srv/" + groupName ).rename( "./SRauncher/srv/" + srvList->objectName() );
}

QString MyServerList::currentServerName() {
	return srvList->currentItem()->text();
}

int MyServerList::currentServerIndex() {
	return srvList->currentRow();
}

stServerInfo MyServerList::currentServerInfo() {
	if ( srvData[currentServerName()] == nullptr ) {
		stServerInfo dummy;
		return dummy;
	}
	return srvData[currentServerName()]->serverInfo();
}

QString MyServerList::currentServerIpPort() {
	if ( srvData[currentServerName()] == nullptr ) {
		return "0.0.0.0:0";
	}
	return srvData[currentServerName()]->serverInfo().address;
}

bool MyServerList::renameCurrentServer( QString name ) {
	if ( srvData.keys().indexOf( name ) == 0 ) return false;
	auto data = srvData[name];
	data->rename( name );
	srvData.remove( name );
	srvData[name] = data;
	return true;
}

void MyServerList::removeCurrentServer( QString name ) {
	delete srvData[name];
	srvData.remove( name );
}

void MyServerList::addNickToCurrentServer( QString nick ) {
	srvData[currentServerName()]->addNick( nick );
}

bool MyServerList::addServer( QString ipPort, QString name ) {
	if ( srvData.keys().indexOf( name ) == 0 ) return false;
	stServerInfo si;
	si.address		 = ipPort;
	si.name			 = name;
	ServerData *data = new ServerData( si, srvList );
	srvList->addItem( name );
	QMessageBox::warning( nullptr, "add server", srvList->objectName() + "::" + name );
	srvData[name] = data;
	return true;
}

void MyServerList::groupIsRenamed( QString name ) {
	settings->sync();
	delete settings;

	QFile( "./SRauncher/srv/" + groupName ).rename( "./SRauncher/srv/" + name );
	settings  = new QSettings( "./SRauncher/srv/" + name, QSettings::IniFormat );
	groupName = name;
}
