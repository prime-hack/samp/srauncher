#include "myitemlist.h"

MyItemList::MyItemList( QWidget *parent ) : QVBoxLayout( parent ) {
	scanDir = "./";
	watch	= new QFileSystemWatcher( QStringList( scanDir ), this );
	QObject::connect( watch, &QFileSystemWatcher::directoryChanged, [&]( const QString &path ) {
		if ( path != scanDir ) return;
		processScan();
	} );
}

void MyItemList::setDir( QString dir ) {
	watch->removePath( scanDir );
	watch->addPath( dir );
	scanDir = dir;
	processScan();
}

QString MyItemList::dir() {
	return scanDir;
}

void MyItemList::setSuffix( QString sfx ) {
	fileSuffix = sfx.toLower();
	processScan();
}

QString MyItemList::suffix() {
	return fileSuffix;
}

void MyItemList::resetLayout() {
	QLayoutItem *item;
	while ( ( item = takeAt( 0 ) ) ) {
		if ( item->widget() ) {
			delete item->widget();
		}
		delete item;
	}
}

void MyItemList::processScan() {
	QDir dir( scanDir );
	if ( !dir.exists() ) return;

	QFileInfoList files = dir.entryInfoList();
	for ( auto &file : files ) {
		if ( !file.isFile() ) {
			files.removeOne( file );
			continue;
		}
		if ( file.suffix().toLower() != fileSuffix ) {
			files.removeOne( file );
			continue;
		}
	}

	QFileInfoList items = itemsList;
	for ( auto &file : items ) {
		if ( files.indexOf( file ) < 0 ) items.removeOne( file );
	}

	for ( auto &file : files )
		if ( items.indexOf( file ) < 0 )
			if ( file.suffix().toLower() == fileSuffix ) items.push_back( file );

	if ( items != itemsList ) {
		resetLayout();
		itemsList.clear();
		itemsList = items;
		for ( auto &file : itemsList ) {
			MyCheckBox *cb = new MyCheckBox( file.completeBaseName(), parentWidget() );
			cb->setChecked( true );
			addWidget( cb );
			emit onAddItem( cb );
		}
		emit onUpdateList();
	}
}
