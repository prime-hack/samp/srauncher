#include "userinterface.h"
#include <QStatusBar>
#include <QFileDialog>
#include <QDesktopWidget>
#include <QVBoxLayout>
#include <QCryptographicHash>
#include "proc/cstart.h"

#include <QMessageBox>

UserInterface::UserInterface( QWidget *parent ) : QMainWindow( parent ) {
	setupUi( this );
	notif->showMessage( "by SR_team", 10000 );

	QDir baseDir( "./SRauncher" );
	if ( !baseDir.exists() )
		if ( !baseDir.mkdir( "./SRauncher" ) ) notif->showMessage( "Can't create dir :(" );

	settings = new QSettings( "./SRauncher/Settings.ini", QSettings::IniFormat, this );

	QDesktopWidget *d = QApplication::desktop();
	setGeometry( settings->value( "SRauncher/pos_x", ( d->width() - 570 ) / 2 ).toInt(),
				 settings->value( "SRauncher/pos_y", ( d->height() - 325 ) / 2 ).toInt(),
				 settings->value( "SRauncher/width", 570 ).toInt(),
				 settings->value( "SRauncher/height", 325 ).toInt() );

	onToggleDevMode( settings->value( "SRauncher/devMode" ).toBool() );

	setSRauncher = new SRauncher( settings->value( "SRauncher/language" ).toString(),
								  settings->value( "SRauncher/devMode" ).toBool(), this );
	QObject::connect( setSRauncher, &SRauncher::onChangeLanguage, this, &UserInterface::onChangeLanguage );
	QObject::connect( setSRauncher, &SRauncher::onToggleDevMode, this, &UserInterface::onToggleDevMode );

	onChangeLanguage( settings->value( "SRauncher/language" ).toString() );

	setSingle = new Single( settings, isAsiLoaderInstalled(), this );

	asiListLayout = new MyItemList( this );
	asi->widget()->setLayout( asiListLayout );

	QObject::connect( asiListLayout, &MyItemList::onAddItem, this, &UserInterface::onAsiItemAdd );

	asiListLayout->setSuffix( "asi" );

	QDir patchDir( "./SRauncher/patches" );
	if ( !patchDir.exists() ) patchDir.mkdir( "./SRauncher/patches" );

	// FIXME: не катит такой вариант. Надо парсить патчи
	patchListLayout = new MyPatchList( this );
	patches->widget()->setLayout( patchListLayout );
	patchListLayout->setSetings( settings, "SRauncher/dev/patch" );
	patchListLayout->setSuffix( "patch" );
	patchListLayout->setDir( "./SRauncher/patches" );

	createGroup = new EditName( this );
	QObject::connect( createGroup, &EditName::onSuccess, this, &UserInterface::onCreateGroup );

	renameGroup = new EditName( this );
	QObject::connect( renameGroup, &EditName::onSuccess, this, &UserInterface::onRenameGroup );

	QStringList srvgroups = settings->value( "SAMP/groups" ).toString().split( ";" );
	for ( auto group : srvgroups ) {
		if ( group.isEmpty() ) continue;

		QListWidget *srvList = new QListWidget( /*tab*/ this );
		srvList->setObjectName( group );
		auto srvs = new MyServerList( srvList, /*tab*/ this );
		srvs->setObjectName( group );
		servers[srvList] = srvs;
		QObject::connect( srvList, &QListWidget::currentRowChanged, this, &UserInterface::onChangeServer );

		groups->addTab( /*scroll*/ srvList, group );
	}
	groups->setCurrentIndex( settings->value( "SAMP/activeGroup" ).toInt() );
}

bool UserInterface::isAsiLoaderInstalled() {
	QFile VorbisFile( "VorbisFile.dll" );
	if ( VorbisFile.exists() ) {
		if ( VorbisFile.open( QIODevice::ReadOnly ) ) {
			QCryptographicHash hash( QCryptographicHash::Md5 );
			hash.addData( VorbisFile.readAll() );
			if ( QString( hash.result().toHex() ) != "2b7b803311d2b228f065c45d13e1aeb2" ) return true;
			VorbisFile.close();
		}
	}
	return false;
}

bool UserInterface::event( QEvent *e ) {
	if ( e->type() == QEvent::Close ) {
		patchListLayout->saveSettings();

		QString srvgroups;
		for ( int i = 0; i < groups->count(); ++i ) srvgroups += groups->widget( i )->objectName() + ";";
		settings->setValue( "SAMP/groups", srvgroups );
		settings->setValue( "SAMP/activeGroup", groups->currentIndex() );

		settings->setValue( "SRauncher/pos_x", pos().x() );
		settings->setValue( "SRauncher/pos_y", pos().y() );
		settings->setValue( "SRauncher/width", size().width() );
		settings->setValue( "SRauncher/height", size().height() );
		delete setSingle;
		delete patchListLayout;
	}
	return QMainWindow::event( e );
}

void UserInterface::changeEvent( QEvent *e ) {
	QMainWindow::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void UserInterface::onChangeLanguage( QString lang ) {
	if ( lang == "Русский" ) {
		translate.load( ":/ru_RU" );
		qApp->installTranslator( &translate );
	} else {
		qApp->removeTranslator( &translate );
	}
	settings->setValue( "SRauncher/language", lang );
}

void UserInterface::onToggleDevMode( bool mode ) {
	if ( mode && isAsiLoaderInstalled() ) {
		groupBox_asi->setVisible( false );
		notif->showMessage( "You already using another ASI-loader" );
	} else
		groupBox_asi->setVisible( mode );
	groupBox_patches->setVisible( mode );
	settings->setValue( "SRauncher/devMode", mode );
}

void UserInterface::onAsiItemAdd( MyCheckBox *cb ) {
	if ( !settings->value( "SRauncher/dev/asi/" + cb->text() ).toString().isEmpty() )
		cb->setChecked( settings->value( "SRauncher/dev/asi/" + cb->text() ).toBool() );
	QObject::connect( cb, &MyCheckBox::lclick, this, &UserInterface::onAsiItemClick );
}

void UserInterface::onAsiItemClick( MyCheckBox *cb ) {
	settings->setValue( "SRauncher/dev/asi/" + cb->text(), cb->isChecked() ^ true );
}

void UserInterface::onCreateGroup( QString name ) {
	for ( int i = 0; i < groups->count(); ++i ) {
		if ( groups->widget( i )->objectName() == name ) {
			notif->showMessage( tr( "Group already exist" ) );
			return;
		}
	}

	QWidget *tab = new QWidget( groups );
	tab->setObjectName( name );
	QScrollArea *scroll = new QScrollArea( groups );
	scroll->setObjectName( name );
	scroll->setWidget( tab );
	QToolBox *srvList = new QToolBox( scroll );
	srvList->setObjectName( name );
	// TODO: insert server list (layout)
	groups->addTab( srvList, name );
}

void UserInterface::onRenameGroup( QString name ) {
	for ( int i = 0; i < groups->count(); ++i ) {
		if ( groups->widget( i )->objectName() == renameGroup->oldName() ) {
			QListWidget *tb = ( (QListWidget *)( (QScrollArea *)groups->widget( i ) )
									->/*widget()->layout()->takeAt(0)->*/ widget() );
			tb->setObjectName( name );
			servers[tb]->groupIsRenamed( name );
			groups->widget( i )->setObjectName( name );
			groups->setTabText( i, name );
			break;
		}
	}
}

void UserInterface::onChangeServer( int index ) {
	QListWidget *tb = (QListWidget *)( (QScrollArea *)groups->currentWidget() );
	if ( tb->count() < index ) {
		notif->showMessage( "Error: Bad server id or group", 10000 );
		return;
	}
	//	QMessageBox::warning(nullptr, "", "onChangeServer(int index)");
	activeSrv = servers[tb];
	nick->setEnabled( true );
	srvConnect->setEnabled( true );
}

void UserInterface::on_actionExit_triggered() {
	close();
}

void UserInterface::on_actionSRauncher_triggered() {
	setSRauncher->show();
}

void UserInterface::on_actionSingle_triggered() {
	setSingle->show();
}

void UserInterface::on_offline_clicked() {
	CStart proc( "gta_sa.exe" );

	if ( settings->value( "Single/asiLoader" ).toBool() || settings->value( "SRauncher/devMode" ).toBool() ) {

		for ( int i = 0; i < asiListLayout->count(); ++i ) {
			MyCheckBox *cb = (MyCheckBox *)asiListLayout->itemAt( i )->widget();

			if ( settings->value( "Single/asiLoader" ).toBool() &&
				 !settings->value( "Single/asi/" + cb->text(), true ).toBool() )
				continue;

			if ( settings->value( "SRauncher/devMode" ).toBool() && !cb->isChecked() ) continue;

			proc.push_library( cb->text() + ".asi" );
		}
	}

	if ( settings->value( "SRauncher/devMode" ).toBool() )
		patchListLayout->applyPatches( proc );
	else
		setSingle->applyPatches( proc );

	proc.execute();
}

void UserInterface::on_debug_clicked() {
	CStart proc( "gta_sa.exe" );

	if ( settings->value( "SRauncher/devMode" ).toBool() ) {

		for ( int i = 0; i < asiListLayout->count(); ++i ) {
			MyCheckBox *cb = (MyCheckBox *)asiListLayout->itemAt( i )->widget();

			if ( settings->value( "SRauncher/devMode" ).toBool() && !cb->isChecked() ) continue;

			proc.push_library( cb->text() + ".asi" );
		}
		patchListLayout->applyPatches( proc );
	}

	proc.push_library( "samp.dll" );
	proc.push_argument( "-d" );
	proc.execute();
}

void UserInterface::on_actionAdd_2_triggered() {
	createGroup->show();
}

void UserInterface::on_actionRename_3_triggered() {
	renameGroup->setOldName( groups->currentWidget()->objectName() );
	renameGroup->show();
}

void UserInterface::on_actionRemove_3_triggered() {
	if ( groups->count() ) {
		if ( QMessageBox::warning( this, tr( "Remove group" ),
								   tr( "Do you really want to delete group \"" ) +
									   groups->currentWidget()->objectName() +
									   tr( "\"?\nThis is not a reversible operation,"
										   " which also removes all the servers from the group!"
										   "\n\nContinue deleting?" ),
								   QMessageBox::Yes | QMessageBox::No ) == QMessageBox::Yes ) {
			groups->removeTab( groups->currentIndex() );
			QListWidget *tb = (QListWidget *)( (QScrollArea *)groups->currentWidget() );
			delete servers[tb];
			servers.remove( tb );
		}
	}
}

void UserInterface::on_actionAdd_triggered() {
	QListWidget *tb = (QListWidget *)( (QScrollArea *)groups->currentWidget() );
	servers[tb]->addServer( "prime-hack.net:1337", "Prime-Hack" );
	// TODO: добавлять не только PH :)
}

void UserInterface::on_srvConnect_clicked() {
	QListWidget *tb = (QListWidget *)( (QScrollArea *)groups->currentWidget() );
	nick->insertItem( 0, nick->currentText() );
	if ( activeSrv->currentServerInfo().nicks.indexOf( nick->currentText() ) == -1 ) {
		servers[tb]->addNickToCurrentServer( nick->currentText() );
	}

	// TODO: run game
	CStart proc( "gta_sa.exe" );

	if ( settings->value( "SRauncher/devMode" ).toBool() ) {

		for ( int i = 0; i < asiListLayout->count(); ++i ) {
			MyCheckBox *cb = (MyCheckBox *)asiListLayout->itemAt( i )->widget();

			if ( settings->value( "SRauncher/devMode" ).toBool() && !cb->isChecked() ) continue;

			proc.push_library( cb->text() + ".asi" );
		}
		patchListLayout->applyPatches( proc );
	}
	// TODO: global settings
	// TODO: server settings

	proc.push_library( "samp.dll" );
	if ( servers[tb]->currentServerInfo().password ) {
		// TODO: password dialog
	}
	proc.push_argument( "-c -n " + nick->currentText() + " -h " + activeSrv->currentServerInfo().ip + " -p " +
						QString::number( activeSrv->currentServerInfo().port ) );
	proc.execute();
}
