#ifndef EDITNAME_H
#define EDITNAME_H

#include "ui_editname.h"

class EditName : public QDialog, private Ui::EditName
{
	Q_OBJECT

public:
	explicit EditName(QWidget *parent = 0);

	virtual void setOldName(QString oldName);
	virtual QString oldName();

protected:
	QString _oldName;

	void changeEvent(QEvent *e);
private slots:
	void on_buttonBox_accepted();

signals:
	void onSuccess(QString name);
};

#endif // EDITNAME_H
