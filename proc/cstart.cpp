#include "cstart.h"
#include <tlhelp32.h>
#include <psapi.h>

CStart::CStart(const QFileInfo &exec, QObject *parent)
	: QObject (parent)
{
	setExecutable(exec);
}

CStart::CStart(const QString &exec, QObject *parent)
	: QObject (parent)
{
	setExecutable(exec);
}

size_t CStart::execute()
{
	log("Process is running...");
	STARTUPINFOA cif;
	ZeroMemory( &cif, sizeof( STARTUPINFOA ) );
	PROCESS_INFORMATION pi;
	if(!CreateProcessA(_exec.fileName().toLocal8Bit(), (char*)_args.join(" ").toLocal8Bit().data(), NULL,NULL, FALSE, DETACHED_PROCESS | CREATE_SUSPENDED, NULL, NULL, &cif, &pi)){
		log("Fail");
		throw std::runtime_error("Failed to create process: " + std::to_string(GetLastError()));
	}
	log("Args: " + _args.join(" ")/*.toLocal8Bit()*/);
	_pid = pi.dwProcessId;
	_handle = pi.hProcess; // мб лучше явно открывать процесс со всеми правами? [Спойлер. Всё и так заебцом]
	for (auto lib : _libs)
		Inject(lib.fileName());
	for (auto set : _sets){
		_activeMemName = set.name;
		memsetEx((void*)((size_t)set.address + (size_t)getLibrary(set.base)), set.value, set.size);
	}
	for (auto cpy : _patches){
		_activeMemName = cpy.name;
		memcpyEx((void*)((size_t)cpy.address + (size_t)getLibrary(cpy.base)), cpy.array);
	}
	_activeMemName.clear();
	ResumeThread(pi.hThread);
	log("Done. Process id " + QString::number(_pid));
	return _pid;
}

void CStart::setExecutable(const QFileInfo &exec)
{
	clear_libraryes();
	clear_memoryOperations();
	_exec = exec;
	_args.clear();
	_pid = 0;
	_handle = nullptr;
}

void CStart::setExecutable(const QString &exec)
{
	setExecutable(QFileInfo(exec));
}

QFileInfo CStart::executable()
{
	return _exec;
}

void CStart::push_argument(const QString &arg)
{
	_args.push_back(arg);
}

void CStart::pop_argument(const QString &arg)
{
	_args.removeOne(arg);
}

void CStart::setArguments(const QStringList &args)
{
	_args.clear();
	for (auto arg : args)
		_args.push_back(arg);
}

void CStart::setArguments(const QString &args)
{
	_args.clear();
	for (auto arg : args.split('\n'))
		if (!arg.isEmpty())
			_args.push_back(arg);
}

QStringList CStart::arguments()
{
	return _args;
}

void CStart::push_library(const QFileInfo &lib)
{
	_libs.push_back(lib);
}

void CStart::push_library(const QString &lib)
{
	push_library(QFileInfo(lib));
}

void CStart::pop_library(const QFileInfo &lib)
{
	_libs.removeOne(lib);
}

void CStart::pop_library(const QString &lib)
{
	pop_library(QFileInfo(lib));
}

void CStart::setLibraryes(const QFileInfoList &libs)
{
	_libs.clear();
	for (auto lib : libs)
		_libs.push_back(lib);
}

QFileInfoList CStart::libraryes()
{
	return _libs;
}

void CStart::clear_libraryes()
{
	_libs.clear();
}

void CStart::addPatch(const stPatch &patch)
{
	_patches.push_back(patch);
}

void CStart::addPatch(void *address, const QByteArray &array)
{
	_patches.push_back(makePatch(address, array));
}

void CStart::addPatch(const size_t &address, const QByteArray &array)
{
	addPatch(reinterpret_cast<void*>(address), array);
}

void CStart::addPatch(void *address, void *buf, size_t size)
{
	QByteArray array;
	for (size_t i = 0; i < size; ++i)
		array.push_back(static_cast<char*>(buf)[i]);
	addPatch(address, array);
}

void CStart::addPatch(const size_t &address, void *buf, size_t size)
{
	addPatch(reinterpret_cast<void*>(address), buf, size);
}

void CStart::addPatch(const QString &base, void *address, const QByteArray &array)
{
	stPatch p = makePatch(address, array);
	p.base = base;
	_patches.push_back(p);
}

void CStart::addPatch(const QString &base, const size_t &address, const QByteArray &array)
{
	addPatch(base, reinterpret_cast<void*>(address), array);
}

void CStart::addPatch(const QString &base, void *address, void *buf, size_t size)
{
	QByteArray array;
	for (size_t i = 0; i < size; ++i)
		array.push_back(static_cast<char*>(buf)[i]);
	addPatch(base, address, array);
}

void CStart::addPatch(const QString &base, const size_t &address, void *buf, size_t size)
{
	addPatch(base, reinterpret_cast<void*>(address), buf, size);
}

void CStart::addSet(const stSet &set)
{
	_sets.push_back(set);
}

void CStart::addSet(void *address, const unsigned char &value, const size_t &size)
{
	_sets.push_back(makeSet(address, value, size));
}

void CStart::addSet(const size_t &address, const unsigned char &value, const size_t &size)
{
	addSet(reinterpret_cast<void*>(address), value, size);
}

void CStart::addSet(const QString &base, void *address, const unsigned char &value, const size_t &size)
{
	stSet s = makeSet(address, value, size);
	s.base = base;
	_sets.push_back(s);
}

void CStart::addSet(const QString &base, const size_t &address, const unsigned char &value, const size_t &size)
{
	addSet(base, reinterpret_cast<void*>(address), value, size);
}

void CStart::addNop(void *address, const size_t &size)
{
	addSet(address, 0x90, size);
}

void CStart::addNop(const size_t &address, const size_t &size)
{
	addSet(address, 0x90, size);
}

void CStart::addNop(const QString &base, void *address, const size_t &size)
{
	addSet(base, address, 0x90, size);
}

void CStart::addNop(const QString &base, const size_t &address, const size_t &size)
{
	addSet(base, address, 0x90, size);
}

void CStart::insertNewMemory(const QByteArray &mem)
{
	stInsMem insmem;
	insmem.array = mem;
}

void *CStart::allocateMemory(uint size)
{
	return VirtualAllocEx( _handle, nullptr, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
}

void CStart::deallocateMemory(void *addr, uint size)
{
	VirtualFreeEx(_handle, addr, size, MEM_RELEASE);
}

DWORD CStart::protectMemmory(void *addr, uint size, DWORD newProtect)
{
	DWORD oldProtect;
	VirtualProtectEx(_handle, addr, size, newProtect, &oldProtect);
	return oldProtect;
}

MEMORY_BASIC_INFORMATION CStart::queryMemory(void *addr)
{
	MEMORY_BASIC_INFORMATION mbi;
	VirtualQueryEx(_handle, addr, &mbi, sizeof(mbi));
	return mbi;
}

void CStart::setMemoryName(QString name)
{
	_activeMemName = name;
}

void CStart::clear_memoryOperations()
{
	_patches.clear();
	_sets.clear();
	_activeMemName.clear();
}

QString CStart::getLog()
{
	return _log.join("\n");
}

size_t CStart::execute(const QFileInfo &exec, const QString &args)
{
	STARTUPINFOA cif;
	ZeroMemory( &cif, sizeof( STARTUPINFOA ) );
	PROCESS_INFORMATION pi;
	if(!CreateProcessA(exec.fileName().toStdString().c_str(), (char*)args.toStdString().c_str(), NULL,NULL, FALSE, DETACHED_PROCESS | CREATE_SUSPENDED, NULL, NULL, &cif, &pi))
		throw std::runtime_error("Failed to create process: " + std::to_string(GetLastError()));
	return pi.dwProcessId;
}

stPatch CStart::makePatch(void *address, const QByteArray &array)
{
	stPatch p;
	if (!_activeMemName.isEmpty())
		p.name = _activeMemName;
	p.address = address;
	p.array = array;
	return p;
}

stSet CStart::makeSet(void *address, const unsigned char &value, const size_t &size)
{
	stSet s;
	if (!_activeMemName.isEmpty())
		s.name = _activeMemName;
	s.address = address;
	s.value = value;
	s.size = size;
	return s;
}

void CStart::log(const QString &text)
{
	_log.push_back("[" + _exec.fileName()/*.toLocal8Bit()*/ + "]: " + text);
}

HMODULE CStart::getLibrary(QString lib)
{
	if (lib.isEmpty())
		return 0;

	log(_activeMemName + " ~> Getting library base " + lib);

	HMODULE hMods[1024];
	DWORD cbNeeded;

	if( EnumProcessModules(_handle, hMods, sizeof(hMods), &cbNeeded)){
		for ( size_t i = 0; i < (cbNeeded / sizeof(HMODULE)); ++i ){
			char szModName[MAX_PATH];
			if ( GetModuleFileNameExA( _handle, hMods[i], szModName, sizeof(szModName)) )
				if (lib.toLower() == QFileInfo(szModName).fileName().toLower())
					return hMods[i];
		}
	}
	return reinterpret_cast<HMODULE>(0x8FFFFFFF);
}

void CStart::Inject(QString dllName)
{
	log("Injecting " + dllName + "...");
	void *LoadLibAddr = (void*)GetProcAddress( GetModuleHandleA( "kernel32.dll" ), "LoadLibraryA" );
	void *dereercomp = VirtualAllocEx( _handle, nullptr, static_cast<size_t>(dllName.length()), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
	WriteProcessMemory( _handle, dereercomp, dllName.toLocal8Bit(), static_cast<size_t>(dllName.length()), nullptr );
	void *remoteThread = CreateRemoteThread( _handle, nullptr, 0, (LPTHREAD_START_ROUTINE)LoadLibAddr, dereercomp, 0, nullptr );
	WaitForSingleObject( remoteThread, INFINITE );
	VirtualFreeEx( _handle, dereercomp, static_cast<size_t>(dllName.length()), MEM_RELEASE );
	CloseHandle( remoteThread );
}

void CStart::memsetEx(void *addr, unsigned char c, size_t size)
{
	if (c != 0x90){
	log(_activeMemName + " ~> Set at " + QString::number(reinterpret_cast<size_t>(addr), 16) +
		", value " + QString::number(c, 16) +
		", size " + QString::number(size, 10));
	} else {
		log(_activeMemName + " ~> Nop at " + QString::number(reinterpret_cast<size_t>(addr), 16) +
			", size " + QString::number(size, 10));
	}
	DWORD dwProtect;
	VirtualProtectEx( _handle, addr, size, PAGE_EXECUTE_READWRITE, &dwProtect );
	for(size_t i = 0; i < size; ++i)
		WriteProcessMemory( _handle, (void*)((uint)addr + i), &c, 1, nullptr );
	VirtualProtectEx( _handle, addr, size, dwProtect, nullptr );
}

QString byteArray2string(const QByteArray &array){
	QString ret = "\"";
	for(auto &ch : array){
		QString buf;
		buf.sprintf("\\x%02X", (unsigned char)ch);
		ret += buf;
	}
	ret += "\"";
	return ret;
}

void CStart::memcpyEx(void *addr, QByteArray array)
{
	log(_activeMemName + " ~> Patch at " + QString::number(reinterpret_cast<size_t>(addr), 16) +
		", array " + byteArray2string(array));
	DWORD dwProtect;
	VirtualProtectEx( _handle, addr, static_cast<size_t>(array.count()), PAGE_EXECUTE_READWRITE, &dwProtect );
	WriteProcessMemory( _handle, addr, array.data(), static_cast<size_t>(array.count()), nullptr );
	VirtualProtectEx( _handle, addr, static_cast<size_t>(array.count()), dwProtect, nullptr );
}
