/*
 * needed libs: user32, kernel32, psapi
 */

#ifndef CSTART_H
#define CSTART_H

#include <QtCore/QObject>
#include <QFileInfo>
#include <QByteArray>
#include <QList>
#include <windows.h>

struct stMemBase{
	void* address;
	QString base = "";
	QString name;
};
struct stPatch : stMemBase{
	QByteArray array;
};
struct stSet : stMemBase{
	unsigned char value;
	size_t size;
};
struct stInsMem : stPatch {}; // Много лишней инфы.

typedef QList<stPatch>	QPatchList;
typedef QList<stSet>	QSetList;
typedef QList<stInsMem>	QInsMemList; // Нужен индетификатор для возврата юзеру. По этому наверно не надо list юзать. Еще надо придумать что-то с relative адресами, раз адрес нет возможности сразу вернуть

class CStart : public QObject
{
	Q_OBJECT
public:
	explicit CStart(const QFileInfo &exec, QObject *parent = nullptr);
	explicit CStart(const QString &exec, QObject *parent = nullptr);
	virtual size_t execute();

	virtual void setExecutable(const QFileInfo &exec);
	virtual void setExecutable(const QString &exec);
	virtual QFileInfo executable();

	virtual void push_argument(const QString & arg);
	virtual void pop_argument(const QString & arg);

	virtual void setArguments(const QStringList &args);
	virtual void setArguments(const QString &args);
	virtual QStringList arguments();

	virtual void push_library(const QFileInfo &lib);
	virtual void push_library(const QString &lib);
	virtual void pop_library(const QFileInfo &lib);
	virtual void pop_library(const QString &lib);

	virtual void setLibraryes(const QFileInfoList &libs);
	virtual QFileInfoList libraryes();
	virtual void clear_libraryes();

	virtual void addPatch(const stPatch &patch);
	virtual void addPatch(void* address, const QByteArray &array);
	virtual void addPatch(const size_t &address, const QByteArray &array);
	virtual void addPatch(void *address, void *buf, size_t size);
	virtual void addPatch(const size_t &address, void *buf, size_t size);
	virtual void addPatch(const QString &base, void* address, const QByteArray &array);
	virtual void addPatch(const QString &base, const size_t &address, const QByteArray &array);
	virtual void addPatch(const QString &base, void *address, void *buf, size_t size);
	virtual void addPatch(const QString &base, const size_t &address, void *buf, size_t size);

	virtual void addSet(const stSet &set);
	virtual void addSet(void* address, const unsigned char &value, const size_t &size);
	virtual void addSet(const size_t &address, const unsigned char &value, const size_t &size);
	virtual void addSet(const QString &base, void* address, const unsigned char &value, const size_t &size);
	virtual void addSet(const QString &base, const size_t &address, const unsigned char &value, const size_t &size);

	virtual void addNop(void* address, const size_t &size);
	virtual void addNop(const size_t &address, const size_t &size);
	virtual void addNop(const QString &base, void* address, const size_t &size);
	virtual void addNop(const QString &base, const size_t &address, const size_t &size);

	// TODO: херня. Переделать
	virtual void insertNewMemory(const QByteArray &mem);

	// FIXME: В таком виде оно годится только для вызова из execute!!!
	// Но ведь и в другом виде оно не сильно юзабельно!!! :( Если только какой-нибудь id юзать
	virtual void *allocateMemory(uint size);
	virtual void deallocateMemory(void* addr, uint size);
	virtual DWORD protectMemmory(void* addr, uint size, DWORD newProtect);
	virtual MEMORY_BASIC_INFORMATION queryMemory(void *addr);

	virtual void setMemoryName(QString name);
	virtual void clear_memoryOperations();

	virtual QString getLog();
	static size_t execute(const QFileInfo &exec, const QString &args = "");

protected:
	virtual stPatch makePatch(void* address, const QByteArray &array);
	virtual stSet makeSet(void* address, const unsigned char &value, const size_t &size);

	virtual void log(const QString &text);

	virtual HMODULE getLibrary(QString lib);

	void Inject(QString dllName);
	void memsetEx(void *addr, unsigned char c, size_t size);
	void memcpyEx(void *addr, QByteArray array);

private:
	QFileInfo		_exec;
	QFileInfoList	_libs;
	QStringList		_args;
	QPatchList		_patches;
	QSetList		_sets;
	QInsMemList		_insmem;
	QStringList		_log;
	size_t			_pid;
	void			*_handle = nullptr;
	QString			_activeMemName;
};

#endif // CSTART_H
