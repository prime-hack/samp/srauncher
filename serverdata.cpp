#include "serverdata.h"
#include <QHostInfo>
#include <QRegExp>

#include <QMessageBox>

template< typename T > T random( T min, T max ) {
	return ( T )( rand() * ( max - min ) ) + min;
}

ServerData::ServerData( stServerInfo info, QObject *parent ) : QObject( parent ) {
	const QRegExp reIpPort( R"(([^:]+):(\d+))" );

	address		= info.address;
	name		= info.name;
	nicks		= info.nicks;
	this->tb	= new QTextBrowser( (QWidget *)parent );
	_isPassword = false;

	if ( reIpPort.indexIn( address ) == 0 ) {
		ip	 = QHostInfo::fromName( reIpPort.cap( 1 ) ).addresses().first();
		port = reIpPort.cap( 2 ).toUShort();
	} else
		throw std::runtime_error( "Invalid Ip:port" );

	localPort = random< ushort >( 2000, 60000 );

	socket = new QUdpSocket( this );
	socket->bind( localPort );
	QObject::connect( socket, &QUdpSocket::readyRead, this, &ServerData::readPendingDatagrams );

	timer = new QTimer( this );
	QObject::connect( timer, &QTimer::timeout, this, &ServerData::update );
	timer->start( 1000 );

	// Header
	header.append( "SAMP" );

	byteValue< quint32 > _ip;
	_ip.value = ip.toIPv4Address();
	for ( int i = 0; i < sizeof( quint32 ); ++i ) header.push_back( _ip.bytes[i] );

	byteValue< ushort > _port;
	_port.value = port;
	for ( int i = 0; i < sizeof( ushort ); ++i ) header.push_back( _port.bytes[i] );

	QMessageBox::warning( nullptr, "new ServerData",
						  ip.toString() + ":" + QString::number( port ) + " == " + address );
}

ServerData::~ServerData() {
	// delete tb;
	timer->stop();
	socket->close();
}

stServerInfo ServerData::serverInfo() {
	stServerInfo si;
	si.name		= name;
	si.nicks	= nicks;
	si.address	= address;
	si.ip		= ip.toString();
	si.port		= port;
	si.password = _isPassword;
	return si;
}

void ServerData::addNick( QString nick ) {
	nicks << nick;
}

void ServerData::rename( QString name ) {
	this->name = name;
}

void ServerData::send( QByteArray data ) {
	socket->writeDatagram( header + data, ip, port );
	timeConnect = QTime::currentTime().msecsSinceStartOfDay();
}

void ServerData::sendPing() {
	QByteArray data;
	data.push_back( QString( "p" + QString::number( localPort ) ).toStdString().c_str() );
	send( data );
}

void ServerData::requestInfo() {
	QByteArray data;
	data.push_back( 'i' );
	send( data );
}

void ServerData::readPendingDatagrams() {
	_ping = QTime::currentTime().msecsSinceStartOfDay() - timeConnect;


	QByteArray buffer;
	buffer.resize( socket->pendingDatagramSize() );

	QHostAddress sender;
	quint16		 senderPort;

	socket->readDatagram( buffer.data(), buffer.size(), &sender, &senderPort );

	if ( buffer[10] == 'i' ) {
		_isPassword = buffer[11];
		int offset	= 12;

		byteValue< ushort > _players;

		_online		 = read< ushort >( buffer, offset );
		_max		 = read< ushort >( buffer, offset );
		QString name = read< QString >( buffer, offset );


		QString info = "Ping: " + QString::number( _ping );
		info += "\nPlayers: " + QString::number( _online ) + " of " + QString::number( _max );
		info += "\nName: " + name;
		// TODO: mode
		info += "\nPassword: " + QString( _isPassword ? "Yes" : "No" );

		tb->setText( info );
	}
}

void ServerData::update() {
	sendPing();
	requestInfo();
}
