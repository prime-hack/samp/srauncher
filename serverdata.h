#ifndef SERVERDATA_H
#define SERVERDATA_H

#include <QtCore/QObject>
#include <QUdpSocket>
#include <QTextBrowser>
#include <QTime>
#include <QTimer>

struct stServerInfo {
	QString		address;
	QString		name;
	QStringList nicks;
	QString		ip;
	ushort		port;
	bool		password;
};

template< typename T > union byteValue {
	T	  value;
	uchar bytes[sizeof( T )];
};

class ServerData : public QObject {
	Q_OBJECT
public:
	explicit ServerData( stServerInfo info, QObject *parent = nullptr );
	~ServerData();

	stServerInfo serverInfo();
	void		 addNick( QString nick );
	void		 rename( QString name );

protected:
	QString		address;
	QString		name;
	QStringList nicks;

	QHostAddress ip;
	ushort		 port;
	ushort		 localPort;

	QTextBrowser *tb;
	QUdpSocket *  socket;
	QTimer *	  timer;

	int timeConnect;

	QByteArray header;

	virtual void send( QByteArray data );

	virtual void sendPing();
	virtual void requestInfo();

	template< typename T > T read( QByteArray arr, int &offset ) {
		byteValue< T > bv;
		for ( int i = 0; i < sizeof( T ); ++i, ++offset ) bv.bytes[i] = arr[offset];
		return bv.value;
	}

private:
	int	 _ping;
	bool _isPassword;
	int	 _online;
	int	 _max;

signals:

public slots:
	void readPendingDatagrams();
	void update();
};
template<> inline QString ServerData::read( QByteArray arr, int &offset ) {
	QString str;
	int		length = read< int >( arr, offset );
	for ( int i = 0; i < length; ++i, ++offset ) str.push_back( QChar( arr[offset] ) );
	return str;
}

#endif // SERVERDATA_H
