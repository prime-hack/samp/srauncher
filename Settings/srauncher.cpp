#include "srauncher.h"
#include <QDir>

SRauncher::SRauncher( QString lang, bool dev, QWidget *parent ) : QDialog( parent ) {
	setupUi( this );

	if ( lang == "Русский" ) lng_ru_RU->setChecked( true );
	devMode->setChecked( dev );
}

void SRauncher::changeEvent( QEvent *e ) {
	QDialog::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void SRauncher::on_devMode_toggled( bool checked ) {
	emit onToggleDevMode( checked );
}

void SRauncher::on_lng_ru_RU_clicked() {
	emit onChangeLanguage( "Русский" );
}

void SRauncher::on_lng_en_EN_clicked() {
	emit onChangeLanguage( "English" );
}
