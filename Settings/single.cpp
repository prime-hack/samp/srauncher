#include "single.h"

Single::Single( QSettings *set, bool hasAnotherAsiLoaderPresent, QWidget *parent ) : QDialog( parent ) {
	setupUi( this );
	this->set	  = set;
	asiListLayout = new MyItemList( this );
	asiList->widget()->setLayout( asiListLayout );

	if ( !hasAnotherAsiLoaderPresent )
		asiLoader->setChecked( set->value( "Single/asiLoader" ).toBool() );
	else
		asiLoader->setEnabled( false );

	QObject::connect( asiListLayout, &MyItemList::onAddItem, this, &Single::onItemAdd );

	asiListLayout->setSuffix( "asi" );

	patchListLayout = new MyPatchList( this );
	patches->widget()->setLayout( patchListLayout );
	patchListLayout->setSetings( set, "Single/patch" );
	patchListLayout->setSuffix( "patch" );
	patchListLayout->setDir( "./SRauncher/patches" );
}

void Single::applyPatches( CStart &proc ) {
	patchListLayout->applyPatches( proc );
}

bool Single::event( QEvent *e ) {
	switch ( e->type() ) {
		case QEvent::Show:
			break;
		case QEvent::Hide:
			patchListLayout->saveSettings();
			break;
		default:
			break;
	}

	return QDialog::event( e );
}

void Single::changeEvent( QEvent *e ) {
	QDialog::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void Single::on_asiLoader_toggled( bool checked ) {
	set->setValue( "Single/asiLoader", checked );
}

void Single::onItemAdd( MyCheckBox *cb ) {
	if ( !set->value( "Single/asi/" + cb->text() ).toString().isEmpty() )
		cb->setChecked( set->value( "Single/asi/" + cb->text() ).toBool() );
	QObject::connect( cb, &MyCheckBox::lclick, this, &Single::onItemClick );
}

void Single::onItemClick( MyCheckBox *cb ) {
	set->setValue( "Single/asi/" + cb->text(), cb->isChecked() ^ true );
}
