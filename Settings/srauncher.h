#ifndef SRAUNCHER_H
#define SRAUNCHER_H

#include "ui_srauncher.h"

class SRauncher : public QDialog, private Ui::SRauncher {
	Q_OBJECT

public:
	explicit SRauncher( QString lang, bool devMode, QWidget *parent = 0 );

protected:
	void changeEvent( QEvent *e );

signals:
	void onChangeLanguage( QString lang );
	void onToggleDevMode( bool mode );

private slots:
	void on_devMode_toggled( bool checked );
	void on_lng_ru_RU_clicked();
	void on_lng_en_EN_clicked();
};

#endif // SRAUNCHER_H
