#ifndef SINGLE_H
#define SINGLE_H

#include <QSettings>
#include "ui_single.h"
#include "../myitemlist.h"
#include "../mypatchlist.h"

class Single : public QDialog, private Ui::Single {
	Q_OBJECT

public:
	explicit Single( QSettings *set, bool hasAnotherAsiLoaderPresent = false, QWidget *parent = nullptr );

	void applyPatches( CStart &proc );

protected:
	bool event( QEvent *e );
	void changeEvent( QEvent *e );

private slots:
	void on_asiLoader_toggled( bool checked );

public slots:
	void onItemAdd( MyCheckBox *cb );
	void onItemClick( MyCheckBox *cb );

signals:

private:
	MyItemList * asiListLayout;
	MyPatchList *patchListLayout;
	QSettings *	 set;
};

#endif // SINGLE_H
