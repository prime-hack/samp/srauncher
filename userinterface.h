#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <QApplication>
#include <QTranslator>
#include <QSettings>
#include <QToolBox>
#include "ui_userinterface.h"
#include "Settings/srauncher.h"
#include "Settings/single.h"
#include "mypatchlist.h"
#include "editname.h"
#include "myserverlist.h"

class UserInterface : public QMainWindow, private Ui::UserInterface {
	Q_OBJECT

public:
	explicit UserInterface( QWidget *parent = nullptr );

	static bool isAsiLoaderInstalled();

protected:
	QTranslator translate;

	bool event( QEvent *e );
	void changeEvent( QEvent *e );

public slots:
	void onChangeLanguage( QString lang );
	void onToggleDevMode( bool mode );
	void onAsiItemAdd( MyCheckBox *cb );
	void onAsiItemClick( MyCheckBox *cb );
	void onCreateGroup( QString name );
	void onRenameGroup( QString name );
	void onChangeServer( int index );

private slots:
	void on_actionExit_triggered();

	void on_actionSRauncher_triggered();

	void on_actionSingle_triggered();

	void on_offline_clicked();

	void on_debug_clicked();

	void on_actionAdd_2_triggered();

	void on_actionRename_3_triggered();

	void on_actionRemove_3_triggered();

	void on_actionAdd_triggered();

	void on_srvConnect_clicked();

private:
	QSettings *settings;
	SRauncher *setSRauncher;
	Single *   setSingle;

	EditName *createGroup;
	EditName *renameGroup;

	EditName *createServer;
	EditName *renameServer;

	QMap< QListWidget *, MyServerList * > servers;

	MyItemList * asiListLayout	 = nullptr;
	MyPatchList *patchListLayout = nullptr;

	MyServerList *activeSrv;
};

#endif // USERINTERFACE_H
