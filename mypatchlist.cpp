#include "mypatchlist.h"
#include <cmath>
#include <cassert>
#include <cstdint>
#include <QDesktopWidget>
#include <QApplication>
#include "FileMan/cfileini.h"
#include <QtWidgets/QDialog>
#include <QtWidgets/QScrollArea>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>

#include <QMessageBox>

void MyPatchList::setSetings( QSettings *set, QString settingsSection ) {
	this->set			  = set;
	this->settingsSection = settingsSection;
}

void MyPatchList::applyPatches( CStart &proc ) {
	saveSettings();
	for ( auto file : itemsList ) {
		CFileIni patch( file.filePath().toLocal8Bit().toStdString() );

		for ( auto p : patches.toStdMap() ) {
			if ( p.second.name != patch.value( "/name" ).c_str() ) continue;
			if ( p.second.desc != patch.value( "/description" ).c_str() ) continue;
			if ( !p.first->isChecked() ) continue;

			auto		  nops = patch.array( "/nop" );
			const QRegExp reNop( R"(([0-9A-F]+)\s+(\d+))", Qt::CaseInsensitive );
			const QRegExp reModuleNop( R"(([^\.]+\.\w+)\s+([0-9A-F]+)\s+(\d+))", Qt::CaseInsensitive );
			for ( auto nop : nops ) {
				if ( reModuleNop.indexIn( nop.c_str() ) == 0 )
					proc.addNop( reModuleNop.cap( 1 ), reModuleNop.cap( 2 ).toUInt( Q_NULLPTR, 16 ),
								 reModuleNop.cap( 3 ).toUInt() );
				else if ( reNop.indexIn( nop.c_str() ) == 0 )
					proc.addNop( reNop.cap( 1 ).toUInt( Q_NULLPTR, 16 ), reNop.cap( 2 ).toUInt() );
			}

			// TODO: apply patches;
			auto		  patchs = patch.array( "/patch" );
			const QRegExp rePatch( R"(([0-9A-F]+)\s+([0-9A-F]+))", Qt::CaseInsensitive );
			const QRegExp reModulePatch( R"(([^\.]+\.\w+)\s+([0-9A-F]+)\s+([0-9A-F]+))",
										 Qt::CaseInsensitive );
			for ( auto patch : patchs ) {
				if ( reModulePatch.indexIn( patch.c_str() ) == 0 ) {
					proc.addPatch( reModulePatch.cap( 1 ), reModulePatch.cap( 2 ).toUInt( Q_NULLPTR, 16 ),
								   stringToBytes( reModulePatch.cap( 3 ) ) );
				} else if ( rePatch.indexIn( patch.c_str() ) == 0 ) {
					proc.addPatch( rePatch.cap( 1 ).toUInt( Q_NULLPTR, 16 ),
								   stringToBytes( rePatch.cap( 2 ) ) );
				} else {
					QString numberPatch = patch.c_str();
					insertVars( numberPatch, p.second.variables );
					//					QMessageBox::warning(parentWidget(), "numberPatch", numberPatch);
					const QRegExp reNumPatch( R"(([0-9A-F]+)\s+(-?\d+))", Qt::CaseInsensitive );
					const QRegExp reNumModulePatch( R"(([^\.]+\.\w+)\s+([0-9A-F]+)\s+(-?\d+))",
													Qt::CaseInsensitive );

					if ( reNumModulePatch.indexIn( numberPatch ) == 0 ) {
						int number = reNumModulePatch.cap( 3 ).toInt();
						proc.addPatch( reNumModulePatch.cap( 1 ),
									   reNumModulePatch.cap( 2 ).toUInt( Q_NULLPTR, 16 ), &number, 4 );
					} else if ( reNumPatch.indexIn( numberPatch ) == 0 ) {
						int number = reNumPatch.cap( 2 ).toInt();
						proc.addPatch( reNumPatch.cap( 1 ).toUInt( Q_NULLPTR, 16 ), &number, 4 );
					}
				}
			}
		}
	}
}

void MyPatchList::saveSettings() {
	for ( auto file : itemsList ) {
		CFileIni patch( file.filePath().toLocal8Bit().toStdString() );

		for ( auto p : patches.toStdMap() ) {
			if ( p.second.name != patch.value( "/name" ).c_str() ) continue;
			if ( p.second.desc != patch.value( "/description" ).c_str() ) continue;

			if ( patches[p.first].dialog != nullptr ) {
				QLayoutItem *item;
				int			 i = 0;
				while ( ( item = patches[p.first].lay->itemAtPosition( i, 1 ) ) ) {
					if ( item->widget() ) {
						QString varName = item->widget()->objectName();

						if ( patches[p.first].variables[varName + ".type"] == "number" ) {
							QSpinBox *spin								   = (QSpinBox *)item->widget();
							patches[p.first].variables[varName + ".value"] = QString::number( spin->value() );

							if ( set != nullptr )
								set->setValue( settingsSection + "/" + p.second.name + "/" + varName +
												   "/value",
											   QString::number( spin->value() ) );
							else
								patch.value( varName.toStdString() + "/value" ) =
									QString::number( spin->value() ).toStdString();
						} else if ( patches[p.first].variables[varName + ".type"] == "select" ) {
							QComboBox *combo							   = (QComboBox *)item->widget();
							patches[p.first].variables[varName + ".value"] = combo->currentText();
							if ( set != nullptr )
								set->setValue( settingsSection + "/" + p.second.name + "/" + varName +
												   "/value",
											   combo->currentText() );
							else
								patch.value( varName.toStdString() + "/value" ) =
									combo->currentText().toStdString();
						}
						// TODO: text
					}
					++i;
				}
			}
		}
		for ( auto p : patches.toStdMap() ) {
			if ( p.second.name != patch.value( "/name" ).c_str() ) continue;
			if ( p.second.desc != patch.value( "/description" ).c_str() ) continue;

			if ( patches[p.first].dialog != nullptr ) {
				QLayoutItem *item;
				int			 i = 0;
				while ( ( item = patches[p.first].lay->itemAtPosition( i, 1 ) ) ) {
					if ( item->widget() ) {
						QString varName = item->widget()->objectName();

						if ( patches[p.first].variables[varName + ".type"] == "select" ) {
							IniValues sec = patch.readSection(
								( varName + "::" + patches[p.first].variables[varName + ".value"] )
									.toStdString() );
							for ( auto &k : sec ) {
								QString variantVarName	= k.first.c_str();
								QString variantVarValue = k.second.front().c_str();
								insertVars( variantVarValue, patches[p.first].variables );
								parseFunc( variantVarValue );
								patches[p.first].variables[varName + "." + variantVarName /*+ "::" + v*/] =
									variantVarValue;
							}
						}
					}
					++i;
				}
			}
		}
	}
}

void MyPatchList::processScan() {
	QDir dir( scanDir );
	if ( !dir.exists() ) return;

	QFileInfoList files = dir.entryInfoList();
	for ( auto &file : files ) {
		if ( !file.isFile() ) {
			files.removeOne( file );
			continue;
		}
		if ( file.suffix().toLower() != fileSuffix ) {
			files.removeOne( file );
			continue;
		}
	}

	QFileInfoList items = itemsList;
	for ( auto &file : items ) {
		if ( files.indexOf( file ) < 0 ) items.removeOne( file );
	}

	for ( auto &file : files )
		if ( items.indexOf( file ) < 0 )
			if ( file.suffix().toLower() == fileSuffix ) items.push_back( file );

	if ( items != itemsList ) {
		resetLayout();
		itemsList.clear();
		itemsList = items;
		for ( auto &file : itemsList ) {
			stMyPatch	p  = parseFile( file.filePath() );
			MyCheckBox *cb = new MyCheckBox( p.name, parentWidget() );
			if ( p.dialog != nullptr ) {
				cb->setIcon( QIcon( ":/Icons/gears.png" ) );
				QObject::connect( cb, &MyCheckBox::rclick, this, &MyPatchList::onOpenSettings );
			}
			QObject::connect( cb, &MyCheckBox::lclick, this, &MyPatchList::onLeftClick );
			cb->setToolTip( p.desc );
			patches[cb] = p;
			if ( set != nullptr )
				cb->setChecked( set->value( settingsSection + "/" + cb->text(), true ).toBool() );
			else
				cb->setChecked( true );
			addWidget( cb );
			emit onAddItem( cb );
		}
		emit onUpdateList();
	}
}

stMyPatch MyPatchList::parseFile( QString file ) {
	stMyPatch result;
	CFileIni  patch( file.toLocal8Bit().toStdString() );
	result.name = patch.value( "/name" ).c_str();
	result.desc = patch.value( "/description" ).c_str();

	result.dialog = nullptr;
	result.lay	  = nullptr;
	int offset	  = 0;
	int oldPos	  = 0;

	auto vars = patch.array( "/var" );
	for ( auto &var : vars ) {
		const QRegExp reVar( R"((\w+)\s+(\d+))" );

		if ( var.empty() ) continue;

		if ( result.dialog == nullptr ) {
			result.dialog = new QDialog( parentWidget() );
			result.dialog->setObjectName( result.name );
			result.dialog->setWindowTitle( result.name );
			result.dialog->resize( 170, 100 );
			QGridLayout *grid = new QGridLayout( result.dialog );
			grid->setHorizontalSpacing( 0 );
			grid->setVerticalSpacing( 0 );
			grid->setContentsMargins( 0, 0, 0, 0 );
			grid->setObjectName( QStringLiteral( "grid" ) );
			QScrollArea *scroll = new QScrollArea( result.dialog );
			scroll->setObjectName( QStringLiteral( "scroll" ) );
			scroll->setWidgetResizable( true );
			QWidget *scrollAreaWidgetContents = new QWidget();
			scrollAreaWidgetContents->setObjectName( QStringLiteral( "scrollAreaWidgetContents" ) );
			scrollAreaWidgetContents->setGeometry( QRect( 0, 0, 168, 98 ) );
			scroll->setWidget( scrollAreaWidgetContents );
			result.lay = new QGridLayout( scrollAreaWidgetContents );
			result.lay->setHorizontalSpacing( 0 );
			result.lay->setVerticalSpacing( 0 );
			result.lay->setContentsMargins( 0, 0, 0, 0 );
			result.lay->setObjectName( QStringLiteral( "baseLayout" ) );
			scrollAreaWidgetContents->setLayout( result.lay );
			grid->addWidget( scroll, 0, 0, 1, 1 );
		}

		QString varName = var.c_str();
		int		varPos	= 0;
		if ( reVar.indexIn( varName ) == 0 ) {
			varName = reVar.cap( 1 );
			varPos	= reVar.cap( 2 ).toInt();
		}

		auto label = new QLabel( varName, result.lay->parentWidget() );
		label->setObjectName( "label_" + varName );
		result.lay->addWidget( label, varPos + offset, 0 );

		result.variables[varName + ".type"] = patch.value( varName.toStdString() + "/type" ).c_str();

		if ( patch.value( varName.toStdString() + "/type" ) == "number" ) {
			auto spin = new QSpinBox( result.lay->parentWidget() );
			spin->setObjectName( varName );
			result.lay->addWidget( spin, varPos + offset, 1 );

			QString min = patch.value( varName.toStdString() + "/min" ).c_str();
			QString max = patch.value( varName.toStdString() + "/max" ).c_str();
			insertVars( min, result.variables );
			parseFunc( min );
			result.variables[varName + ".min"] = min;
			spin->setMinimum( min.toInt() );
			insertVars( max, result.variables );
			parseFunc( max );
			result.variables[varName + ".max"] = max;
			spin->setMaximum( max.toInt() );

			QString value = patch.value( varName.toStdString() + "/value" ).c_str();
			if ( set != nullptr )
				value = set->value( settingsSection + "/" + result.name + "/" + varName + "/value",
									patch.value( varName.toStdString() + "/value" ).c_str() )
							.toString();
			insertVars( value, result.variables );
			parseFunc( value );
			result.variables[varName + ".value"] = value;
			spin->setValue( value.toInt() );
			if ( min.toInt() > 0 ) spin->setSingleStep( min.toInt() );
		} else if ( patch.value( varName.toStdString() + "/type" ) == "select" ) {
			auto combo = new QComboBox( parentWidget() );
			combo->setObjectName( varName );
			result.lay->addWidget( combo, varPos + offset, 1 );

			QString		variant	 = patch.value( varName.toStdString() + "/variant" ).c_str();
			QStringList variants = variant.split( ";" );
			combo->addItems( variants );

			QString value = patch.value( varName.toStdString() + "/value" ).c_str();
			if ( set != nullptr )
				value = set->value( settingsSection + "/" + result.name + "/" + varName + "/value",
									patch.value( varName.toStdString() + "/value" ).c_str() )
							.toString();
			combo->setCurrentText( value );
			result.variables[varName + ".value"] = value;
		}
		// TODO: parse type == text

		if ( varPos == oldPos ) ++offset;
		oldPos = varPos;
	}

	return result;
}

void MyPatchList::insertVars( QString &value, QMap< QString, QString > &vars ) {
	QDesktopWidget *d = QApplication::desktop();
	value.replace( "$display.x", QString::number( d->width() ) );
	value.replace( "$display.y", QString::number( d->height() ) );

	for ( auto &v : vars.toStdMap() ) {
		const QRegExp re( R"((\w+)\.(\w+))" );
		if ( v.first.isEmpty() ) continue;

		QString var = v.first;
		//		if (re.indexIn(var) == 0){
		//			if (vars[re.cap(1) + ".type"] == "select" && re.cap(2) != "type" && re.cap(2) != "variant" &&
		//re.cap(2) != "value"){ 				value.replace(re.cap(1) + "." + re.cap(2), var);
		//			}
		//		}

		value.replace( "$" + var, v.second );
	}
}

QByteArray MyPatchList::stringToBytes( QString str ) {
	QByteArray result;
	if ( str.length() % 2 == 1 ) str.push_front( '0' );

	for ( int i = 0; i < str.length(); i += 2 ) {
		QString strByte = str.at( i );
		strByte += str.at( i + 1 );

		byte b = strByte.toUInt( Q_NULLPTR, 16 );
		result.push_back( b );
	}

	return result;
}

template< int N > struct Table {
	constexpr Table() : t() {
		t[0] = 1;
		for ( auto i = 1; i < N; ++i ) t[i] = t[i - 1] * i;
	}
	std::uint64_t t[N];
};

template< typename T > T fac( T x ) {
	static_assert( sizeof( T ) <= sizeof( std::uint64_t ), "T is too large" );
	constexpr auto table = Table< 66 >();
	assert( x >= 0 );
	return x < 66 ? static_cast< T >( table.t[x] ) : 0;
}

void MyPatchList::parseFunc( QString &f ) {
	QString carretFunc;
	int		carretId = 0;
	for ( int i = 0; i < f.length(); ++i ) {
		if ( f[i] == ')' ) {
			--carretId;
			if ( carretId < 0 ) return;
			if ( carretId == 0 ) {
				QString precalc = carretFunc;
				if ( carretFunc.indexOf( "(" ) != -1 ) parseFunc( carretFunc );
				calc( carretFunc );
				f.replace( "(" + precalc + ")", carretFunc );
				carretFunc.clear();
			}
		}
		if ( carretId ) carretFunc.push_back( f[i] );
		if ( f[i] == '(' ) {
			++carretId;
		}
	}
	while ( true ) {
		QString old = f;
		calc( f );
		if ( f == old ) break;
	}
}

void MyPatchList::calc( QString &f ) {
	const QRegExp _add( R"((-?\d+([,.]\d+f?)?)\s*\+\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _sub( R"((-?\d+([,.]\d+f?)?)\s*\-\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _mul( R"((-?\d+([,.]\d+f?)?)\s*\*\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _div( R"((-?\d+([,.]\d+f?)?)\s*\/\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _pow( R"((-?\d+([,.]\d+f?)?)\s*\^\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _and( R"((-?\d+([,.]\d+f?)?)\s*\&\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _or( R"((-?\d+([,.]\d+f?)?)\s*\|\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _xor( R"((-?\d+([,.]\d+f?)?)\s*\#\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _mod( R"((-?\d+([,.]\d+f?)?)\s*\%\s*(-?\d+([,.]\d+f?)?))" );
	const QRegExp _sqrt( R"(v(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _log( R"(l(-?\d+([,.]\d+f?)?)g(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _lng( R"(ln(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _ldg( R"(lg(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _lbg( R"(lb(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _cos( R"(cos(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _sin( R"(sin(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _tg( R"(tg(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _ctg( R"(ctg(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _acos( R"(acos(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _asin( R"(asin(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _atg( R"(atg(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _actg( R"(actg(-?\d+([,.]\d+f?)?))", Qt::CaseInsensitive );
	const QRegExp _fac( R"((-?\d+([,.]\d+f?)?)!)" );

	const QRegExp _exp( R"(e)", Qt::CaseInsensitive );
	const QRegExp _pi( R"(p)", Qt::CaseInsensitive );

	// Constants
	while ( _exp.indexIn( f ) != -1 ) {
		f.replace( _exp.cap( 0 ), "2.71828" );
	}

	while ( _pi.indexIn( f ) != -1 ) {
		f.replace( _pi.cap( 0 ), "3.14159" );
	}

	// Matches
	while ( _log.indexIn( f ) != -1 ) {
		double	op1	   = _log.cap( 1 ).toDouble();
		double	op2	   = _log.cap( 3 ).toDouble();
		QString result = QString::number( log( op2 ) / log( op1 ) );
		f.replace( _log.cap( 0 ), result );
	}

	while ( _lng.indexIn( f ) != -1 ) {
		double	op	   = _lng.cap( 1 ).toDouble();
		QString result = QString::number( log( op ) );
		f.replace( _lng.cap( 0 ), result );
	}

	while ( _ldg.indexIn( f ) != -1 ) {
		double	op	   = _ldg.cap( 1 ).toDouble();
		QString result = QString::number( log10( op ) );
		f.replace( _ldg.cap( 0 ), result );
	}

	while ( _lbg.indexIn( f ) != -1 ) {
		double	op	   = _lbg.cap( 1 ).toDouble();
		QString result = QString::number( log2( op ) );
		f.replace( _lbg.cap( 0 ), result );
	}

	while ( _acos.indexIn( f ) != -1 ) {
		double	op	   = _acos.cap( 1 ).toDouble();
		QString result = QString::number( acos( op ) );
		f.replace( _acos.cap( 0 ), result );
	}

	while ( _asin.indexIn( f ) != -1 ) {
		double	op	   = _asin.cap( 1 ).toDouble();
		QString result = QString::number( asin( op ) );
		f.replace( _asin.cap( 0 ), result );
	}

	while ( _atg.indexIn( f ) != -1 ) {
		double	op	   = _atg.cap( 1 ).toDouble();
		QString result = QString::number( atan( op ) );
		f.replace( _atg.cap( 0 ), result );
	}

	while ( _actg.indexIn( f ) != -1 ) {
		double	op	   = _actg.cap( 1 ).toDouble();
		QString result = QString::number( 1.0f / atan( op ) );
		f.replace( _actg.cap( 0 ), result );
	}

	while ( _cos.indexIn( f ) != -1 ) {
		double	op	   = _cos.cap( 1 ).toDouble();
		QString result = QString::number( cos( op ) );
		f.replace( _cos.cap( 0 ), result );
	}

	while ( _sin.indexIn( f ) != -1 ) {
		double	op	   = _sin.cap( 1 ).toDouble();
		QString result = QString::number( sin( op ) );
		f.replace( _sin.cap( 0 ), result );
	}

	while ( _tg.indexIn( f ) != -1 ) {
		double	op	   = _tg.cap( 1 ).toDouble();
		QString result = QString::number( tan( op ) );
		f.replace( _tg.cap( 0 ), result );
	}

	while ( _ctg.indexIn( f ) != -1 ) {
		double	op	   = _ctg.cap( 1 ).toDouble();
		QString result = QString::number( 1.0f / tan( op ) );
		f.replace( _ctg.cap( 0 ), result );
	}

	while ( _mod.indexIn( f ) != -1 ) {
		int		op1	   = _mod.cap( 1 ).toInt();
		int		op2	   = _mod.cap( 3 ).toInt();
		QString result = QString::number( op1 % op2 );
		f.replace( _mod.cap( 0 ), result );
	}

	while ( _and.indexIn( f ) != -1 ) {
		int		op1	   = _and.cap( 1 ).toInt();
		int		op2	   = _and.cap( 3 ).toInt();
		QString result = QString::number( op1 & op2 );
		f.replace( _and.cap( 0 ), result );
	}

	while ( _or.indexIn( f ) != -1 ) {
		int		op1	   = _or.cap( 1 ).toInt();
		int		op2	   = _or.cap( 3 ).toInt();
		QString result = QString::number( op1 | op2 );
		f.replace( _or.cap( 0 ), result );
	}

	while ( _xor.indexIn( f ) != -1 ) {
		int		op1	   = _xor.cap( 1 ).toInt();
		int		op2	   = _xor.cap( 3 ).toInt();
		QString result = QString::number( op1 ^ op2 );
		f.replace( _xor.cap( 0 ), result );
	}

	while ( _pow.indexIn( f ) != -1 ) {
		double op1 = _pow.cap( 1 ).toDouble();
		double op2 = _pow.cap( 3 ).toDouble();
		if ( op1 < 0 && op2 != (long long)op2 ) // WARNING: bypass complex numbers as in KCalc
			op1 *= -1;
		QString result = QString::number( pow( op1, op2 ) );
		f.replace( _pow.cap( 0 ), result );
	}

	while ( _fac.indexIn( f ) != -1 ) {
		double	op	   = _fac.cap( 1 ).toDouble();
		QString result = QString::number( fac< std::uint64_t >( op ) );
		f.replace( _fac.cap( 0 ), result );
	}

	while ( _mul.indexIn( f ) != -1 ) {
		double	op1	   = _mul.cap( 1 ).toDouble();
		double	op2	   = _mul.cap( 3 ).toDouble();
		QString result = QString::number( op1 * op2 );
		f.replace( _mul.cap( 0 ), result );
	}

	while ( _div.indexIn( f ) != -1 ) {
		double	op1	   = _div.cap( 1 ).toDouble();
		double	op2	   = _div.cap( 3 ).toDouble();
		QString result = QString::number( op1 / op2 );
		f.replace( _div.cap( 0 ), result );
	}

	while ( _mod.indexIn( f ) != -1 ) {
		int		op1	   = _mod.cap( 1 ).toInt();
		int		op2	   = _mod.cap( 3 ).toInt();
		QString result = QString::number( op1 % op2 );
		f.replace( _mod.cap( 0 ), result );
	}

	while ( _add.indexIn( f ) != -1 ) {
		double	op1	   = _add.cap( 1 ).toDouble();
		double	op2	   = _add.cap( 3 ).toDouble();
		QString result = QString::number( op1 + op2 );
		f.replace( _add.cap( 0 ), result );
	}

	while ( _sub.indexIn( f ) != -1 ) {
		double	op1	   = _sub.cap( 1 ).toDouble();
		double	op2	   = _sub.cap( 3 ).toDouble();
		QString result = QString::number( op1 - op2 );
		f.replace( _sub.cap( 0 ), result );
	}

	while ( _sqrt.indexIn( f ) != -1 ) {
		double	op	   = _sqrt.cap( 1 ).toDouble();
		QString result = QString::number( sqrt( op ) );
		f.replace( _sqrt.cap( 0 ), result );
	}
}

void MyPatchList::onOpenSettings( MyCheckBox *cb ) {
	if ( patches[cb].dialog != nullptr ) patches[cb].dialog->show();
}

void MyPatchList::onLeftClick( MyCheckBox *cb ) {
	if ( set == nullptr ) return;

	set->setValue( settingsSection + "/" + cb->text(), cb->isChecked() ^ true );
}
