<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>EditName</name>
    <message>
        <location filename="editname.ui" line="14"/>
        <source>Insert name</source>
        <translation>Введите название</translation>
    </message>
    <message>
        <location filename="editname.ui" line="24"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
</context>
<context>
    <name>SRauncher</name>
    <message>
        <location filename="Settings/srauncher.ui" line="14"/>
        <source>SRauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/srauncher.ui" line="20"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="Settings/srauncher.ui" line="41"/>
        <source>Русский</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/srauncher.ui" line="48"/>
        <source>En&amp;glish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/srauncher.ui" line="92"/>
        <source>Update scan:</source>
        <translation>Интервал скана:</translation>
    </message>
    <message>
        <location filename="Settings/srauncher.ui" line="101"/>
        <source>Developer mode</source>
        <translation>Режим разработчика</translation>
    </message>
</context>
<context>
    <name>Single</name>
    <message>
        <location filename="Settings/single.ui" line="14"/>
        <source>Single</source>
        <translation>Одиночка</translation>
    </message>
    <message>
        <location filename="Settings/single.ui" line="30"/>
        <source>Asi Loader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Settings/single.ui" line="58"/>
        <source>Patches</source>
        <translation>Патчи</translation>
    </message>
</context>
<context>
    <name>UserInterface</name>
    <message>
        <location filename="userinterface.ui" line="14"/>
        <source>SRauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="85"/>
        <source>AsiLoader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="126"/>
        <source>Patches</source>
        <translation>Патчи</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="178"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="189"/>
        <source>Debug</source>
        <translation>Отладка</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="234"/>
        <source>Single</source>
        <translation>Одиночка</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="259"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="266"/>
        <source>&amp;Import</source>
        <translation>&amp;Импорт</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="282"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="289"/>
        <source>&amp;Settings</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="301"/>
        <source>S&amp;ervers</source>
        <translation>С&amp;ервера</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="313"/>
        <source>&amp;Groups</source>
        <translation>&amp;Группы</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="328"/>
        <source>Abo&amp;ut</source>
        <translation>&amp;О</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="351"/>
        <source>&amp;Open patch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="354"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="363"/>
        <source>&amp;New patch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="366"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="375"/>
        <source>&amp;from SAMP</source>
        <translation>&amp;из SAMP</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="378"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="387"/>
        <source>from &amp;older SRauncher</source>
        <translation>&amp;из старого SRauncher</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="403"/>
        <source>&amp;Exit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="406"/>
        <source>Close this program</source>
        <translation>Закрыть программу</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="409"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="418"/>
        <source>&amp;SRauncher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="421"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="426"/>
        <source>&amp;Global</source>
        <translation>&amp;Глобальные</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="429"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="434"/>
        <source>S&amp;erver</source>
        <translation>С&amp;ервер</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="437"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="442"/>
        <source>&amp;Forum</source>
        <translation>&amp;Форум</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="445"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="450"/>
        <source>&amp;Add server</source>
        <translation>&amp;Добавить сервер</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="453"/>
        <source>F10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="458"/>
        <source>&amp;Rename server</source>
        <translation>&amp;Переименовать сервер</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="461"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="466"/>
        <source>R&amp;emove server</source>
        <translation>У&amp;далить сервер</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="471"/>
        <source>&amp;Add group</source>
        <translation>&amp;Добавить группу</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="474"/>
        <source>F9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="479"/>
        <source>&amp;Rename group</source>
        <translation>&amp;Переименовать группу</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="484"/>
        <source>Remove &amp;group</source>
        <translation>Удалить &amp;группу</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="489"/>
        <source>&amp;Move server</source>
        <translation>&amp;Переместить сервер</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="492"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="497"/>
        <source>&amp;Add tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="502"/>
        <source>&amp;Remove tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="507"/>
        <source>&amp;Edit tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="516"/>
        <source>&amp;About SRauncher</source>
        <translation>&amp;О SRauncher</translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="519"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userinterface.ui" line="524"/>
        <source>S&amp;ingle</source>
        <translation>О&amp;диночка</translation>
    </message>
    <message>
        <location filename="userinterface.cpp" line="169"/>
        <source>Group already exist</source>
        <translation>Группа уже существует</translation>
    </message>
    <message>
        <location filename="userinterface.cpp" line="289"/>
        <source>Remove group</source>
        <translation>Удаление группы</translation>
    </message>
    <message>
        <location filename="userinterface.cpp" line="290"/>
        <source>Do you really want to delete group &quot;</source>
        <translation>Вы действитеьно хотите удалить группу &quot;</translation>
    </message>
    <message>
        <location filename="userinterface.cpp" line="292"/>
        <source>&quot;?
This is not a reversible operation, which also removes all the servers from the group!

Continue deleting?</source>
        <translation>&quot;?
Эта операция так же приведет к удалению всех серверов из этой группы!

Продолжить удаление?</translation>
    </message>
</context>
</TS>
