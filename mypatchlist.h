#ifndef MYPATCHLIST_H
#define MYPATCHLIST_H

#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QSettings>
#include "myitemlist.h"
#include "proc/cstart.h"

struct stMyPatch{
	QString name;
	QString desc;
	QMap<QString, QString> variables;
	QDialog *dialog; // TODO: show dialog on rclick
	QGridLayout *lay;
};

class MyPatchList : public MyItemList
{
public:
	explicit MyPatchList(QWidget *parent = nullptr) : MyItemList(parent){}

	virtual void setSetings(QSettings *set, QString settingsSection);

	virtual void applyPatches(CStart &proc);
	virtual void saveSettings();

protected:
	QMap<MyCheckBox*, stMyPatch> patches;

	virtual void processScan();
	virtual stMyPatch parseFile(QString file);
	virtual void insertVars(QString &value, QMap<QString, QString> &vars);

	virtual QByteArray stringToBytes(QString str);

	// form EasyCalc
	virtual void parseFunc(QString &f);
	virtual void calc(QString &f);

private:
	QSettings *set = nullptr;
	QString settingsSection;

private slots:
	void onOpenSettings(MyCheckBox *cb);
	void onLeftClick(MyCheckBox *cb);
};

#endif // MYPATCHLIST_H
