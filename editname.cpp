#include "editname.h"

EditName::EditName(QWidget *parent) :
	QDialog(parent)
{
	setupUi(this);
}

void EditName::setOldName(QString oldName)
{
	_oldName = oldName;
	name->setText(_oldName);
}

QString EditName::oldName()
{
	return _oldName;
}

void EditName::changeEvent(QEvent *e)
{
	QDialog::changeEvent(e);
	switch (e->type()) {
		case QEvent::LanguageChange:
			retranslateUi(this);
			break;
		default:
			break;
	}
}

void EditName::on_buttonBox_accepted()
{
	emit onSuccess(name->text());
	name->clear();
}
