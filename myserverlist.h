#ifndef MYSERVERLIST_H
#define MYSERVERLIST_H

#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QListWidget>
#include <QSettings>
#include <QMap>
#include "serverdata.h"

class MyServerList : public QObject
{
	Q_OBJECT
public:
	explicit MyServerList(QListWidget* srvList, QObject *parent = nullptr);
	virtual ~MyServerList();

	virtual QString currentServerName();
	virtual int currentServerIndex();
	virtual stServerInfo currentServerInfo();
	virtual QString currentServerIpPort();

	virtual bool renameCurrentServer(QString name); // false if exist
	virtual void removeCurrentServer(QString name);
	virtual void addNickToCurrentServer(QString nick);

	virtual bool addServer(QString ipPort, QString name); // false if exist

protected:
	QListWidget *srvList;
	QSettings *settings;
	QMap<QString, ServerData*> srvData;

private:
	QString groupName;

signals:

public slots:
	void groupIsRenamed(QString name);
};

#endif // MYSERVERLIST_H
