#-------------------------------------------------
#
# Project created by QtCreator 2017-10-26T17:35:59
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SRauncher
TEMPLATE = app
#CONFIG -= UNICODE _UNICODE
#QMAKE_CXXFLAGS += -fexec-charset=CP1251
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
		main.cpp \
		userinterface.cpp \
	mycheckbox.cpp \
	FileMan/cfilebase.cpp \
	FileMan/cfilebin.cpp \
	FileMan/cfileinfo.cpp \
	FileMan/cfileini.cpp \
	FileMan/cfiletext.cpp \
	proc/cstart.cpp \
	Settings/srauncher.cpp \
	Settings/single.cpp \
	myitemlist.cpp \
	mypatchlist.cpp \
	editname.cpp \
	myserverlist.cpp \
    serverdata.cpp

HEADERS += \
		userinterface.h \
	mycheckbox.h \
	FileMan/cfilebase.h \
	FileMan/cfilebin.h \
	FileMan/cfileinfo.h \
	FileMan/cfileini.h \
	FileMan/cfiletext.h \
	proc/cstart.h \
	Settings/srauncher.h \
	Settings/single.h \
	myitemlist.h \
	mypatchlist.h \
	editname.h \
	myserverlist.h \
    serverdata.h

FORMS += \
		userinterface.ui \
	Settings/srauncher.ui \
	Settings/single.ui \
	editname.ui

TRANSLATIONS += ru_RU.ts

DISTFILES +=

RESOURCES += \
	resources.qrc

RC_FILE = resource.rc

LIBS += -luser32 \
		-lkernel32 \
		-lpsapi
