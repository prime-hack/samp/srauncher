#ifndef MYCHECKBOX_H
#define MYCHECKBOX_H

#include <QCheckBox>
#include <QMouseEvent>

class MyCheckBox : public QCheckBox
{
	Q_OBJECT
public:
	explicit MyCheckBox(QWidget *parent = Q_NULLPTR) : QCheckBox(parent) {}
	explicit MyCheckBox(const QString &text, QWidget *parent = Q_NULLPTR) : QCheckBox(text, parent) {}

protected:
	virtual void mousePressEvent(QMouseEvent *e);

signals:
	void lclick(MyCheckBox *cb);
	void mclick(MyCheckBox *cb);
	void rclick(MyCheckBox *cb);
};

#endif // MYCHECKBOX_H
