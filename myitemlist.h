#ifndef MYITEMLIST_H
#define MYITEMLIST_H

#include <QtCore/QObject>
#include <QTimer>
#include <QVBoxLayout>
#include <QDir>
#include <QFileSystemWatcher>
#include "mycheckbox.h"

class MyItemList : public QVBoxLayout {
	Q_OBJECT
public:
	explicit MyItemList( QWidget *parent = nullptr );

	virtual void	setDir( QString dir );
	virtual QString dir();

	virtual void	setSuffix( QString sfx );
	virtual QString suffix();

protected:
	QFileSystemWatcher *watch;
	QFileInfoList		itemsList;
	QString				scanDir;
	QString				fileSuffix;

	virtual void resetLayout();

	virtual void addLayout( QLayout *layout, int stretch = 0 ) {}

	virtual void processScan();

signals:
	void onAddItem( MyCheckBox *cb );
	void onUpdateList();
};

#endif // MYITEMLIST_H
