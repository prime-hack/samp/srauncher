#include "mycheckbox.h"


void MyCheckBox::mousePressEvent(QMouseEvent *e)
{
	if (e->button() == Qt::LeftButton)
		emit lclick(this);
	if (e->button() == Qt::MiddleButton)
		emit rclick(this);
	if (e->button() == Qt::RightButton)
		emit rclick(this);
	QCheckBox::mousePressEvent(e);
}
